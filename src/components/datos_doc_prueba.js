export const datos_doc=[
    {
        campo:"Apellidos",
        valor:"Hernández Chávez",
    },
    {
        campo:"Nombres",
        valor:"Salvador Enrique",
    },
    {
        campo:"Género",
        valor:"Masculino",
    },
    {
        campo:"Salvadoreño por",
        valor:"nacimiento",
    },
    {
        campo:"Fecha y Lugar de Nacimiento",
        valor:"29/09/1995 San Salvador, San Salvador",
    },
    {
        campo:"Fecha y Lugar de Expedición",
        valor:"04/11/2021 Soyapango, San Salvador",
    },
    {
        campo:"fecha de Expiración",
        valor:"03/11/2029",
    },
    {
        campo:"Numero Unico de Identidad",
        valor:"05246377-7",
    },
    {
        campo:"Residencia",
        valor:"Los Santos Coruña 2 Pje 3 BK N casa 20",
    },
    {
        campo:"Tramite",
        valor:"RN-1",
    },
    {
        campo:"Municipio",
        valor:"Soyapango",
    },
    {
        campo:"Departamento",
        valor:"San Salvador",
    },
    {
        campo:"Nombre de la Madre",
        valor:"Nancy Lissette Chávez García",
    },
    {
        campo:"Nombre del Padre",
        valor:"Ricardo Amilcar Hernández Reinoza",
    },
    {
        campo:"Código de Zona",
        valor:"101040000T",
    },
    {
        campo:"Estado Familiar",
        valor:"Soltero",
    },
    {
        campo:"NIT",
        valor:"",
    },
    {
        campo:"Tipo Sangre",
        valor:"",
    },
    {
        campo:"Profesión Oficio",
        valor:"Estudiante",
    },
]