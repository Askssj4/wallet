
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/cuenta', component: () => import('pages/Perfil.vue') },
      { path: '/tarjetas_bancarias', component: () => import('pages/TarjetasBancarias.vue')},
      { path: '/tarjetas', component: () => import('pages/Tarjetas.vue')},
      { path: '/documentos', component: () => import('pages/Documentos.vue')},
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
